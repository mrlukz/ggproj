# README #

GGPROJ jest symulatorem zakupu i sprzedaży towarów, w tym konkretnym przypadku przygotowany pod sprzedaż owoców i warzyw. Symuluje wpisy w książce przychodów i rozchodów która musi być prowadzona przez małych sprzedawców nie korzystających z kas fiskalnych. 

Wpisy przychodów są generowane na podstawie zaznaczonej opcji 'Kup', natomiast wpisy rozchodów generowane są losowo, symulując 'podejścia' klientów do momentu zejścia danego towaru ze stanu.

W pliku lista.xml znajduje się przykładowa baza z towarami na których można dokonać symulacji.